package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
)

const (
	cells   = 100 // 网格数
	xyrange = 30.0
	angle   = math.Pi / 6 // 角度
)

var sin30, cos30 = math.Sin(angle), math.Cos(angle)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		vars := r.URL.Query()
		width := vars.Get("width")
		widthInt, err := strconv.ParseInt(width, 10, 64)
		if err != nil {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprintf(w, "<html><body>必须添加宽度，形如<a href=\"http://localhost:8000/?width=1080&height=980&color=blue\">http://localhost:8000/?width=1080&height=980&color=blue</a></body></html>")
			return
		}
		height := vars.Get("height")
		heightInt, err := strconv.ParseInt(height, 10, 64)
		if err != nil {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprintf(w, "<html><body>必须添加高度，形如<a href=\"http://localhost:8000/?width=1080&height=980&color=blue\">http://localhost:8000/?width=1080&height=980&color=blue</a></body></html>")
			return
		}
		color := vars.Get("color")
		if len(color) == 0 { //默认红色
			color = "red"
		}
		surface(w, widthInt, heightInt, color)
	})
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func surface(out http.ResponseWriter, width int64, height int64, color string) {
	out.Header().Set("Content-Type", "image/svg+xml")
	fmt.Fprintf(out, "<svg xmlns='http://www.w3.org/2000/svg'"+
		" style='stroke: %s;fill:white;stroke-width:0.7'"+
		" width='%d' height='%d'>", color, width, height)
	for i := 0; i < cells; i++ {
		for j := 0; j < cells; j++ {
			ax, ay := corner(i+1, j, width, height)
			bx, by := corner(i, j, width, height)
			cx, cy := corner(i, j+1, width, height)
			dx, dy := corner(i+1, j+1, width, height)

			if math.IsNaN(ax) || math.IsNaN(ay) || math.IsNaN(bx) || math.IsNaN(by) || math.IsNaN(cx) || math.IsNaN(cy) || math.IsNaN(dx) || math.IsNaN(dy) {
				// 跳过无效的多边形
				continue
			}

			fmt.Fprintf(out, "<polygon points='%g,%g %g,%g %g,%g %g,%g'/>\n",
				ax, ay, bx, by, cx, cy, dx, dy)

		}
	}
	fmt.Fprintln(out, "</svg>")
}

func corner(i, j int, width int64, height int64) (float64, float64) {
	// 求出网格单元（i, j）的顶点坐标
	x := xyrange * (float64(i)/cells - 0.5)
	y := xyrange * (float64(j)/cells - 0.5)

	zscale := float64(height) * 0.4
	xyscale := float64(width) / 2 / xyrange
	// 计算曲面高度
	z := f(x, y)

	// 将(x,y,z) 等角投射到 2维 SVG 平面， 坐标为(sx, sy)
	sx := float64(width/2) + (x-y)*cos30*xyscale
	sy := float64(height/2) + (x+y)*sin30*xyscale - z*zscale
	return sx, sy
}

func f(x, y float64) float64 {
	r := math.Hypot(x, y) // 到(0,0)的距离
	return math.Sin(r) / r
}
