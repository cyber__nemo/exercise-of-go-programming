# exerciseOfGoProgramming

#### 介绍

用于保存《Go程序设计语言》的练习代码

#### 第一章
主要介绍了一些简单的示例代码。
完成了练习1.12。

![运行结果](pic/ex_1_12.gif)

#### 第二章
主要讲解了命名、声明、变量、赋值、类型声明和简单的包管理。

##### 命名
Go语言命名要求字母或下划线开头，区分大小写，需要回避25个关键字，并需要注意一些预声明的变量。推荐使用驼峰命名。

##### 声明
主要包括变量（var）、常量(const)、类型(type)、函数(func)。

##### 变量
可以使用var声明，或使用“:=”短声明，还可以使用new函数。

##### 赋值
使用“=”赋值，Go支持多重赋值“a, b = 1, 2”。

##### 类型声明
type定义一个新的命名类型，与某个已有类型使用相同的底层类型，至少会提供一种独有的方法来区分。

##### 包管理
使用“import”引入包。可以使用“go mod”进行包管理。

完成了练习2-2，包括了类型声明，重写了float64类型，并引入了本仓库的包。

#### 第三章
Go主要包括基础类型、聚合类型、引用类型和接口类型。
基础类型包括数字、字符串和布尔类型。聚合类型包括数组和结构体。引用类型包括指针、切片、map、函数和channel。

完成了练习3-4，完成了一个简单的服务器返回SVG图像。

#### 第四章 复合数据类型

##### 数组
长度固定的拥有零个或多个相同数据类型的序列，初始值为该元素0值，不同长度数组为不同数据类型。
初始化方法`var array [3]int`，`array := [...]int{1,2,3}`，`array := [...]int{99:1}`。
Go语言中数组使用值传递。
可以使用“==”进行比较。

##### slice
拥有相同数据类型的变长序列，三个属性：指针，长度（len），容量(cap)。
零值为nil。
[]byte与string相似。
Go函数应该以相同的方式处理长度为0的slice，无论是否为nil。
底层数组的扩容机制。
对slice进行处理后都应更新slice变量。

##### map
键值对的无序集合。map是散列表的引用，类型为map[k]v，k,v分别对应键和值的数据类型。k必须可使用==进行比较。
在零值map中设置元素会导致错误。
可以利用map实现集合。

##### 结构体
结构体是一种聚合的数据类型，是由零个或多个任意类型的值聚合成的实体。每个值称为结构体的成员。
结构体变量的成员可以通过点操作符访问。
如果结构体成员名字是以大写字母开头的，那么该成员就是可导出的。